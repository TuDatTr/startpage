use dioxus::prelude::*;

pub fn Footer(cx: Scope) -> Element {
    cx.render(rsx!(div { class: "footer", "This page had 123 visitors." }))
}
