use dioxus::prelude::*;
use std::fmt::{Display, Formatter, Result};

mod links;
use links::{Link, Links};

#[derive(PartialEq, Props)]
pub struct FullName<'a> {
    first_name: &'a str,
    last_name: &'a str,
    user_name: &'a str,
    nick_name: &'a str,
}

impl<'a> FullName<'a> {
    pub fn new(
        first_name: &'a str,
        last_name: &'a str,
        user_name: &'a str,
        nick_name: &'a str,
    ) -> Self {
        Self {
            first_name,
            last_name,
            user_name,
            nick_name,
        }
    }
}

impl<'a> Display for FullName<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "{}", self.user_name)
    }
}
pub fn Body(cx: Scope) -> Element {
    let name: FullName = FullName::new("Tuan-Dat", "Tran", "TuDatTr", "Tuan");
    let gitlab: Link = Link {
        name: "GitLab".to_string(),
        url: "https://gitlab.com/TuDatTr".to_string(),
        icon: "".to_string(),
    };
    let homepage: Link = Link {
        name: "Personal Page".to_string(),
        url: "https://tudattr.dev".to_string(),
        icon: "".to_string(),
    };

    let links: Vec<Link> = vec![homepage, gitlab];
    let links_ref = use_ref(cx, Vec::<Link>::new);

    for l in links {
        println!("{:?}", l);
        links_ref.write().push(l);
    }

    let links_lock = links_ref.read();
    // let links_rendered = links_lock.iter().map(|l| {
    //     rsx!(Links {
    //         name: "{l.name}".to_string(),
    //         url: "{l.url}".to_string(),
    //         icon: "{l.icon}".to_string()
    //     })
    // });

    cx.render(rsx!(div {
        class: "body",
        // links_rendered
    }))
}
