use startpage::App;

fn main() {
    // launch the web app
    dioxus_web::launch(App);
}
