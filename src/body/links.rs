use dioxus::prelude::*;

#[derive(PartialEq, Props, Debug)]
pub struct Link {
    pub name: String,
    pub url: String,
    pub icon: String,
}

pub fn Links(cx: Scope<Link>) -> Element {
    cx.render(rsx!(a {
        class: "button",
        href: "{cx.props.url}",
        "{cx.props.icon}: cx.props.name"
    }))
}
