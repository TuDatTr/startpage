#![allow(non_snake_case)]

use std::fmt::{Display, Formatter, Result};

use dioxus::prelude::*;

mod header;
use header::Header;

mod body;
use body::Body;

mod footer;
use footer::Footer;

pub struct Link<'a> {
    name: &'a str,
    url: &'a str,
    icon: &'a str,
}

#[derive(PartialEq, Props)]
pub struct FullName<'a> {
    first_name: &'a str,
    last_name: &'a str,
    user_name: &'a str,
    nick_name: &'a str,
}

impl<'a> FullName<'a> {
    pub fn new(
        first_name: &'a str,
        last_name: &'a str,
        user_name: &'a str,
        nick_name: &'a str,
    ) -> Self {
        Self {
            first_name,
            last_name,
            user_name,
            nick_name,
        }
    }
}

impl<'a> Display for FullName<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "{}", self.user_name)
    }
}
pub fn App(cx: Scope) -> Element {
    cx.render(rsx! {
        Header{},
        Body{},
        Footer{},
    })
}
