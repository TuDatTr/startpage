use dioxus::prelude::*;

pub fn Header(cx: Scope) -> Element {
    cx.render(rsx!(div { class: "header", "" }))
}
