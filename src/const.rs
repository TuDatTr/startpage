struct Link {
    name: String,
    url: String,
    icon: String,
}

const NAME: String = "TuDatTr";
const SOCIALS: Vec<Link> = {
    let homepage = Link {
        name: "Personal Page",
        url: "https://tudattr.dev",
        icon: "",
    };
    let gitlab = Link {
        name: "GitLab",
        url: "https://gitlab.com/TuDatTr",
        icon: "",
    };
    vec![homepage, gitlab]
};
